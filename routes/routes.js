'use strict';
var request 	 = require("request");
const octopage 	 = require('github-pagination');
const bodyParser = require('body-parser');





module.exports = function(app) {

	

	app.get('/', function(req, res) {
	    res.render('home');
	});


	app.get('/github', function(req, res) {
	    res.render('github');
	});

	app.post('/github', function(req, res) {
		var searchString = req.body.name;
    	res.redirect('/github/results/' + searchString + '/');
	});

	// app.get('/bitbucket', function(req, res) {
	//     res.render('home');
	// });

	// app.get('/gitlab', function(req, res) {
	//     res.render('home');
	// });	




	

	//  GITHUB  //	
	//			//
	//			//
	//			//
	
	app.get('/github/results/:searchString', function(req, res) {
		var query = req.params.searchString;
		var select = req.query.selectpicker;
	    const endpoint = 'https://api.github.com/search/repositories?q='
	    				 + query + '&per_page=' + select + '&sort=stars&order=desc+';

	    request.get(endpoint, { headers: { "User-Agent": "request" } }, function(error, response, body) {
			if(!error && response.statusCode == 200) {
				var githubData = JSON.parse(body);
	      		var testLinks = response.headers.link;
	      		var paginationLinks = octopage.parser(testLinks);
	      		res.render("./github/results", {githubData: githubData, paginationLinks: paginationLinks });

			}
	      }
	    );
	});


	app.get('/github/results/:searchString/:id', function(req, res) {
		var query = req.params.searchString;
		var pageNumber = '&page=' + req.params.id;
		var select = req.query.selectpicker;
	    const endpoint = 'https://api.github.com/search/repositories?q='
	    				 + query + '&per_page=' + select +'&sort=stars&order=desc+' + pageNumber;

	    request.get(endpoint, { headers: { "User-Agent": "request" } }, function(error, response, body) {
			if(!error && response.statusCode == 200) {
				var githubData = JSON.parse(body);
	      		var testLinks = response.headers.link;
	      		var paginationLinks = octopage.parser(testLinks);
	      		res.render("./github/results", {githubData: githubData, paginationLinks: paginationLinks });

			}
	      }
	    );
	});




};