// Require packages
var express 	= require('express'),
	bodyParser 	= require('body-parser'),
  	port 		= process.env.PORT || 3000,
 	app  		= express();


//





// Use Body-parser package
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
//



// Use EJS for rendering views
app.set("view engine", "ejs");
//



// Importing the routes
var routes = require('./routes/routes'); 
// 



// Register the routes in the app
routes(app);
//


app.listen(port);


// Start Server
console.log('Git Search RESTful API server started on: ' + port);
